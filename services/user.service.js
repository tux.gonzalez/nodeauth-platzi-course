const pool = require('../libs/postgres.pool');
const boom = require('@hapi/boom');
const bcrypt = require('bcrypt');

const { models } = require('./../libs/sequelize');

class UserService {
  constructor() {
    this.pool = pool;
    this.pool.on('error', err => console.log(err));
  }

  async create(data) {
    const hash = await bcrypt.hash(data.password, 10)
    const newUser =  await models.User.create({
      ...data,
      password: hash
    })
    delete newUser.dataValues.password;
    return newUser;
  }

  async find() {
    const response = await models.User.findAll({
      include: ['customer']
    });
    return response;
  }

  async findOne(id) {
    const user = await models.User.findByPk(id);
    if(!user) {
      throw boom.notFound('User not found')
    }
    return user;
  }

  async findByEmail(email) {
    const user = await models.User.findOne({
      where: { email }
    });
    return user;
  }

  async update(id, changes) {
    const user = await this.findOne(id);
    const response = user.update(changes)
    return response;
  }

  async delete(id) {
    const user = await this.findOne(id);
    await user.destroy();
    return { id };
  }
}

module.exports = UserService;
