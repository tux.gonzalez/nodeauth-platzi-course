const bcrypt = require('bcrypt');

const hashPassword = async() => {
  const myPassword = 'p@ssw0rd';
  const hash =  await bcrypt.hash(myPassword, 10)
  console.log("🚀 ~ file: pass-hash.js:6 ~ hashPassword ~ hash:", hash)
};


hashPassword();
