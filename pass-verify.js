const bcrypt = require('bcrypt');

const verifyPassword = async () => {
  const myPassword = 'p@ssw0rd';
  const hash = '$2b$10$9eRB1qLqwUhIwwuJJrAJp.RBNkc1cbo7wq1hFbDMj2rKJ9nAonUOe'
  const isMatch = await bcrypt.compare(myPassword, hash)
  console.log("🚀 ~ file: pass-verify.js:7 ~ hashPassword ~ isMatch:", isMatch)
};


verifyPassword();
